from selenium.webdriver.common.by import By
from pageObjects.account_setup_pages.account_setup_page import AccountSetupPage
from selenium.webdriver.support.wait import WebDriverWait


class LoggingNavigationBar:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    # elements in the navigation bar
    swoogo_logo = (By.CSS_SELECTOR, ".navbar-brand")
    dashboard = (By.XPATH, "//a[normalize-space()='Dashboard']")
    events_dropdown = (By.LINK_TEXT, "Events")
    view_events = (By.CSS_SELECTOR, "//a[normalize-space()='View Events']")
    create_event = (By.XPATH, "//a[normalize-space()='Create Event']")
    cross_event_reports = (By.XPATH, "//a[normalize-space()='Cross Event Reports']")
    contacts_dropdown = (By.CSS_SELECTOR, "//a[@class='dropdown-toggle'][normalize-space()='Contacts']")
    view_contacts = (By.XPATH, "//a[normalize-space()='View Contacts']")
    create_contact = (By.XPATH, "//a[normalize-space()='Create Contact']")
    contacts_report = (By.XPATH, "//a[normalize-space()='Contact Reports']")
    recent_events = (By.XPATH, "//a[normalize-space()='Recent']")
    search_input = (By.CSS_SELECTOR, "#search-form input[name='SearchForm[query]']")
    search_dropdown = (By.CSS_SELECTOR, "//button[@class='btn btn-default dropdown-toggle']")
    search_bar_event = (By.XPATH, "//span[@class='object']//following::a[@href='#'][normalize-space()='Events']")
    search_bar_registrants = (By.XPATH, "//span[@class='object']//following::a[@href='#'][normalize-space()='Registrants']")
    search_bar_contacts = (By.XPATH, "//span[@class='object']//following::a[@href='#'][normalize-space()='Contacts']")
    support_dropdown = (By.XPATH, "//a[normalize-space()='Support']")
    support_dropdown_helpcenter = (By.LINK_TEXT, "Online Help Center")
    support_dropdown_myrequests = (By.LINK_TEXT, "My Requests")
    support_dropdown_contactus = (By.LINK_TEXT, "Contact Us")
    profile_dropdown = (By.ID, "profile-tab")
    my_profile = (By.XPATH, "//a[normalize-space()='My Profile']")
    switch_account = (By.CSS_SELECTOR, ".switch")
    account_setup = (By.XPATH, "//a[normalize-space()='Account Setup']")
    logout = (By.CSS_SELECTOR, ".logout")

    def get_all_elements(self):
        navbar_elements = {
            'swoogo_logo': self.driver.find_element(*LoggingNavigationBar.swoogo_logo),
            'dashboard': self.driver.find_element(*LoggingNavigationBar.dashboard),
            'events_dropdown': self.driver.find_element(*LoggingNavigationBar.events_dropdown),
            'view_events': self.driver.find_element(*LoggingNavigationBar.view_events),
            'create_event': self.driver.find_element(*LoggingNavigationBar.create_event),
            'cross_event_reports': self.driver.find_element(*LoggingNavigationBar.cross_event_reports),
            'contacts_dropdown': self.driver.find_element(*LoggingNavigationBar.contacts_dropdown),
            'view_contacts': self.driver.find_element(*LoggingNavigationBar.view_contacts),
            'create_contact': self.driver.find_element(*LoggingNavigationBar.create_contact),
            'contacts_report': self.driver.find_element(*LoggingNavigationBar.contacts_report),
            'recent_events': self.driver.find_element(*LoggingNavigationBar.recent_events),
            'search_input': self.driver.find_element(*LoggingNavigationBar.search_input),
            'search_dropdown': self.driver.find_element(*LoggingNavigationBar.search_dropdown),
            'search_bar_event': self.driver.find_element(*LoggingNavigationBar.search_bar_event),
            'search_bar_registrants': self.driver.find_element(*LoggingNavigationBar.search_bar_registrants),
            'search_bar_contacts': self.driver.find_element(*LoggingNavigationBar.search_bar_contacts),
            'support_dropdown': self.driver.find_element(*LoggingNavigationBar.support_dropdown),
            'support_dropdown_helpcenter': self.driver.find_element(*LoggingNavigationBar.support_dropdown_helpcenter),
            'support_dropdown_myrequests': self.driver.find_element(*LoggingNavigationBar.support_dropdown_myrequests),
            'support_dropdown_contactus': self.driver.find_element(*LoggingNavigationBar.support_dropdown_contactus),
            'profile_dropdown': self.driver.find_element(*LoggingNavigationBar.profile_dropdown),
            'my_profile': self.driver.find_element(*LoggingNavigationBar.my_profile),
            'switch_account': self.driver.find_element(*LoggingNavigationBar.switch_account),
            'account_setup': self.driver.find_element(*LoggingNavigationBar.account_setup),
            'logout': self.driver.find_element(*LoggingNavigationBar.logout)
        }
        return navbar_elements

    def find_element(self, locator):
        pass

    def dynamic_search(self, text, search_option=None):
        if search_option is None:
            self.driver.find_element(*LoggingNavigationBar.search_input).send_keys(text)
        else:
            search_option = getattr(LoggingNavigationBar, search_option)
            self.driver.find_element(*search_option).click()
            self.driver.find_element(*LoggingNavigationBar.search_input).send_keys(text)

    def create_an_event(self):
        pass

    def create_a_contact(self):
        pass

    def add_dashlet(self):
        pass

    def manage_dashboard(self):
        pass

    def switch_dashboards(self):
        pass

    def logout_of_account(self):
        pass

    def switch_to_account(self):
        pass

    # this is how to return page objects from entry points in test cases
    def go_to_account_setup(self):
        self.driver.find_element(*LoggingNavigationBar.profile_dropdown).click()
        self.driver.find_element(*LoggingNavigationBar.account_setup).click()
        account_setup_page = AccountSetupPage(self.driver)
        return account_setup_page

    def go_to_profile_page(self):
        pass
