from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from utilities.base_class import BaseClass
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains


class MFAPage(BaseClass):
    current_device_button = (By.XPATH, "//a[contains(text(), 'Deactivate Current Device')]")

    def __init__(self, driver, eyes, configuration):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)
        self.eyes = eyes

    def deactivate_current_device(self):
        log = self.getLogger()
        try:
            current_device_btn = self.driver.find_element(*self.current_device_button)
            self.verify_element_clickable(By.XPATH, "//a[contains(text(), 'Deactivate Current Device')]")
            ActionChains(self.driver).scroll_to_element(current_device_btn)
            current_device_btn.click()
            self.verify_element_located(By.XPATH, "//button[@data-confirm-delete]")
            self.eyes.check_window('after delete button is clicked')
            result = self.eyes.close(False)
        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            raise
        else:
            log.info('Dashboard has Deactivated')
        return result
