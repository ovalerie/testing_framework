from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException

from utilities.base_class import BaseClass


class AccountDashboardPage(BaseClass):
    dashboard_page = (By.CSS_SELECTOR, 'body')
    add_dashlet_button = (By.XPATH, "//a[normalize-space()='Add Dashlet']")
    daily_stats_dashlet = (By.XPATH, "//button[contains(@onclick, 'daily_stats')]")
    upcoming_events_dashlet = (By.XPATH, "//button[contains(@onclick, 'upcoming_events')]")
    latest_updates_dashlet = (By.XPATH, "//button[contains(@onclick, 'latest')]")
    reg_by_regions_dashlet = (By.XPATH, "//button[contains(@onclick, 'geographic')]")
    favorites_event_dashlet = (By.XPATH, "//button[contains(@onclick, 'favorite_events')]")
    feature_requests_dashlet = (By.XPATH, "//button[contains(@onclick, 'feature_requests')]")

    def __init__(self, driver, eyes, configuration):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)
        self.eyes = eyes

    def add_dashlet(self, widget_type: tuple, widget_name=None):
        widget_name = widget_type[1][29:]
        log = self.getLogger()
        data_id = None
        try:
            self.driver.find_element(*self.add_dashlet_button).click()
            self.wait.until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "modal_0")))
            self.driver.find_element(*widget_type).click()
            self.driver.switch_to.default_content()
            self.driver.refresh()
            new_dashlet = self.driver.find_element(By.CSS_SELECTOR, ".dash-panel")
            data_id = new_dashlet.get_attribute("data-id")
        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.error("Unexpected error: %s", e)
            raise
        else:
            log.info("Successfully added dashlet of type: %s", widget_name)
        return data_id

    def get_dashlet(self):
        log = self.getLogger()
        try:
            dashlet = self.driver.find_element(By.CSS_SELECTOR, ".dash-panel")
        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            raise
        else:
            log.info("Successfully retrieved dashlet")
        return dashlet

    def get_dashboard(self):
        log = self.getLogger()
        self.driver.find_element(By.XPATH, "//button[text()='Default View ']").click()
        self.driver.find_element(By.XPATH, "//a[text()='Manage Dashboards']").click()
        try:
            delete_btns = self.driver.find_elements(By.CSS_SELECTOR, ".delete")
            for btn in delete_btns:
                if "disabled" in btn.get_attribute('class'):
                    pass
                else:
                    dashboard = btn.find_element(By.XPATH, "ancestor::tr[@data-key]")
                    return dashboard
        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            raise

    def delete_dashlet(self, data_id):
        log = self.getLogger()
        if data_id is None:
            raise ValueError("data_id should not be None")
        try:
            dashlet = self.driver.find_element(By.XPATH, f"//div[@data-id ='{data_id}']")
            self.verify_element_clickable(By.CSS_SELECTOR, ".pull-right")
            dashlet.find_element(By.CSS_SELECTOR, ".pull-right").click()
            self.verify_element_clickable(By.CSS_SELECTOR, ".delete-dashlet")
            self.driver.find_element(By.CSS_SELECTOR, ".delete-dashlet").click()
            self.verify_element_located(By.XPATH, "//button[@data-confirm-delete]")
            self.eyes.check_window('after delete button is clicked')
            result = self.eyes.close(False)
            # # self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#delete_confirmation .modal-footer")))
            # h4_text = self.driver.find_element(By.XPATH, "//h4[contains(text(), 'Are you sure you want to delete this')]").text.strip()
            # assert h4_text == "Are you sure you want to delete this Dashlet?"
            # self.driver.find_element(By.XPATH, "//button[normalize-space()='Yes, delete it']").click()
            # self.wait.until(EC.invisibility_of_element_located(dashlet))

        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            raise
        else:
            log.info('Dashlet has been deleted')
        return result

    def delete_dashboard(self, data_key):
        log = self.getLogger()
        if data_key is None:
            raise ValueError("data_key should not be None")
        try:
            dashboard = self.driver.find_element(By.XPATH, f"//tr[@data-key ='{data_key}']")
            self.verify_element_clickable(By.CSS_SELECTOR, ".delete")
            dashboard.find_element(By.CSS_SELECTOR, ".delete").click()
            self.verify_element_located(By.XPATH, "//button[@data-confirm-delete]")
            self.eyes.check_window('after delete button is clicked and modal is triggered')
            result = self.eyes.close(False)
        except NoSuchElementException as e:
            log.error("Element not found: %s", e)
            raise
        except TimeoutException as e:
            log.error("Operation timed out: %s", e)
            raise
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            raise
        else:
            log.info('Dashboard has been deleted')
        return result

    def create_custom_dashboard(self):
        pass
