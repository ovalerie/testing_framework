import pytest
from applitools.selenium import MatchLevel
from dotenv import load_dotenv
from utilities.base_class import BaseClass
from pageObjects.account_dashboard_page import AccountDashboardPage
from pageObjects.my_profile.mfa_page import MFAPage
from selenium.webdriver.common.by import By

load_dotenv()


@pytest.mark.usefixtures("webdriver", "eyes", "configuration")
class TestEyesDemo(BaseClass):

    def show_ui_difference(self, webdriver, eyes, configuration):
        self.driver = webdriver
        log = self.getLogger()
        eyes.match_level = MatchLevel.LAYOUT
        self.driver.get("https://www.swoogo-qa3.com/loggedin/custom-domain/index")
        try:
            btns = self.driver.find_elements(By.CLASS_NAME, "delete")
            for btn in btns:
                if "disabled" not in btn.get_attribute('class'):
                    btn.click()
                    self.verify_element_located(By.XPATH, "//button[@data-confirm-delete]")
                    eyes.check_window("after delete modal is triggered")
                    result = eyes.close(False)
                    self.handle_result(result)
                    break
                else:
                    pass
        except Exception as e:
            log.exception("Unexpected error: %s", e)
            pytest.fail("Unexpected error: {}".format(e))

    def test_dashboard_dashlet(self, webdriver, eyes, configuration):
        first_test = False
        self.driver = webdriver
        log = self.getLogger()
        eyes.match_level = MatchLevel.LAYOUT
        eyes.test_name = "test_dashboard_dashlet"
        self.login()

        if first_test:
            self.driver.get("https://www.swoogo-qa3.com/loggedin/dashboard/index")
            try:
                dashboard = AccountDashboardPage(self.driver, eyes, configuration)
                dashlet = dashboard.get_dashlet()
                data_id = dashlet.get_attribute("data-id")
                result = dashboard.delete_dashlet(data_id)
                self.handle_result(result)
            except Exception as e:
                log.exception("Unexpected error: %s", e)
                pytest.fail("Unexpected error: {}".format(e))
        else:
            self.show_ui_difference(self.driver, eyes, configuration)

    def test_dashboard_delete(self, webdriver, eyes, configuration):
        first_test = False
        self.driver = webdriver
        log = self.getLogger()
        eyes.match_level = MatchLevel.LAYOUT
        eyes.test_name = "test_dashboard_delete"
        if first_test:
            self.driver.get("https://www.swoogo-qa3.com/loggedin/dashboard/index")
            try:
                self.driver.refresh()
                dashboard = AccountDashboardPage(self.driver, eyes, configuration)
                custom_dashboard = dashboard.get_dashboard()
                data_key = custom_dashboard.get_attribute("data-key")
                result = dashboard.delete_dashboard(data_key)
                self.handle_result(result)
            except Exception as e:
                log.exception("Unexpected error: %s", e)
                pytest.fail("Unexpected error: {}".format(e))
        else:
            self.show_ui_difference(self.driver, eyes, configuration)

    #
    def test_my_profile_mfa(self, webdriver, eyes, configuration):
        first_test = True
        self.driver = webdriver
        log = self.getLogger()
        eyes.match_level = MatchLevel.LAYOUT
        eyes.test_name = "test_my_profile_mfa"
        if first_test:
            self.driver.get("https://www.swoogo-qa3.com/loggedin/user-mfa-method/index")
            try:
                profile_page = MFAPage(self.driver, eyes, configuration)
                result = profile_page.deactivate_current_device()
                self.handle_result(result)
            except Exception as e:
                log.exception("Unexpected error: %s", e)
                pytest.fail("Unexpected error: {}".format(e))
        else:
            self.show_ui_difference(self.driver, eyes, configuration)

    def handle_result(self, result):
        log = self.getLogger()
        try:

            url = result.url
            total_steps = result.steps
            if result.is_new:
                result_str = f"New baseline image created: {str(total_steps)} steps"
                log.info(result_str)
            elif result.is_passed:
                result_str = f"All steps passed: {str(total_steps)}"
                log.info(result_str)
            else:
                result_str = "Test Failed     :     " + str(total_steps) + " steps"
                result_str += " matches=" + str(result.matches)  # matched the baseline
                result_str += " missing=" + str(result.missing)  # missing in the test
                result_str += " mismatches=" + str(result.mismatches)  # did not match the baseline
                log.info(result_str)

            result_str += "\n" + "results at " + url
            log.info(result_str)
            log.info(result)
        except Exception as e:
            log.exception("An error occurred in handle_result: " + str(e))
            print("results handled.")
