import json
import logging
import os
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from abc import ABC, abstractmethod


class BaseClass(ABC):
    def login(self):
        log = self.getLogger()
        self.driver.get("https://www.swoogo-qa3.com/site/login")
        self.driver.refresh()
        if self.driver.current_url == "https://www.swoogo-qa3.com/site/login":
            try:
                self.driver.find_element(By.ID, "loginform-email").send_keys(os.getenv('EMAIL'))
                self.driver.find_element(By.ID, "loginform-password").send_keys(os.getenv('PASSWORD'))
                self.driver.find_element(By.XPATH, "//button[@name='login-button']").click()

                try:
                    skip_mfa = self.driver.find_element(By.CSS_SELECTOR, "skip_link_btn")
                    if skip_mfa.is_displayed() and skip_mfa.is_enabled():
                        skip_mfa.click()
                        log.info("Logged In!")
                    else:
                        pass
                except NoSuchElementException as e:
                    log.info(e.msg)
            except NoSuchElementException as e:
                log.error("Element not found: %s", e)
            except TimeoutException as e:
                log.error("Operation timed out: %s", e)
            except Exception as e:
                log.exception("Unexpected error: %s", e)
        else:
            log.info("Login Skipped.")

    def verify_element_located(self, locator, text):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((locator, text)))

    def verify_element_clickable(self, locator, text):
        element = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((locator, text)))

    def getLogger(self):
        logger_name = 'swoogo_test'  # Use a consistent name for the logger
        logger = logging.getLogger(logger_name)

        if not logger.handlers:  # Only add the handler if it doesn't already have one
            file_handler = logging.FileHandler('logfile.log')
            formatter = logging.Formatter(
                "%(asctime)s : %(levelname)s : %(filename)s : %(module)s : %(funcName)s : %(lineno)d : %(message)s",
                datefmt="%m-%d-%Y %I:%M:%S %p")
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
            logger.setLevel(logging.DEBUG)

        return logger

    def static_search(self, locator, text):
        pass

    def count_elements(self, element_locator):
        log = self.getLogger()
        elements = self.driver.find_elements(By.CSS_SELECTOR, element_locator)
        return len(elements)

    # @abstractmethod
    # def delete_elements(self):
    #     pass

    def view_element_details(self):
        pass

    def clone_element(self):
        pass

    def update_element(self):
        pass
